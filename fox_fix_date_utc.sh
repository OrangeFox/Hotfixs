#!/bin/bash
#
# Fix the ro.build.date.utc bug in Pie, by setting it to an old date
#
# date --date='@1533963106' = Sat Aug 11 05:51:40 BST 2018
#

if [ "$2" != "--last-call" ]; then
DEFAULT_PROP=$1/default.prop
DT="1533963106" # must be less than 1533963226
echo "#*******************************************************"
echo "- Running callback, with parameter \"$1\""
echo "- Patching $DEFAULT_PROP ..."
sed -i -e "s/ro.build.date.utc=.*/ro.build.date.utc=$DT/g" $DEFAULT_PROP
echo "- Finished working around other people's bugs!"
echo "#*******************************************************"
exit 0
fi
